var express= require('express'),
app=express(),
bodyParser = require('body-parser'),
fs = require('fs');
//mysql = require('mysql');
var mqtt = require('mqtt');
var db = require('./mysqldb');
var Sequelize = require('sequelize');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/blazedb";
var request = require("request");

// DB DETAILS 

var MySqlPort = 3306;
var MySqlDB = "BlazeDB";
var MySqlUser = "root";
//Current production server
var MySqlHost = 'localhost'; // '54.86.166.212';
var MySqlPass = "bl@z3";
var dbo ;

MongoClient.connect(url,  { useNewUrlParser: true },function(err, db) {
    if (err) throw err;
    console.log("Database connected!");
    dbo = db.db("blazedb");
   // db.close();
  });

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });

app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

process.on('SIGINT', function() {
    process.exit(0);
    
  });
process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log(err.stack);
  });

  var options ={
	port: 1884,
	host: 'mqtt://smartmqtt.b1automation.com',
	clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
	username:"mqttcli",
	password:"lsmqtt",
	keepalive: 5,
	reconnectPeriod: 3000,
	protocolId: 'MQTT',
	qos:2,
	queueQoSZero:false,
	resubscribe:true
	};

    var  client =mqtt.connect('mqtt://smartmqtt.b1automation.com',options);
  

 // DOc : mysql db connection 

 db.connect(MySqlHost, MySqlPort, MySqlDB, MySqlUser, MySqlPass, function (err) {

    console.log("mysql  connection  "+MySqlHost + ': ' + MySqlPort);
    if (err) {
     console.log('Unable to connect to MySql.');
      process.exit(1);
      client.end();
    } else {
        console.log('Connected to the database');
        app.listen(3000, function () {

            console.log("connecting to the port 3000");
           
        
          }); 
    }
  
  });


  // DOC: mqtt connection 

  client.on('connect',function(connack){
  
	console.log("connected to mqtt" + JSON.stringify(connack));	
	client.subscribe('test/learn');
	client.subscribe('mqtt/devapp/devid_B4E62DDCFE51/fromdev/predict');

});

// DOC: Subscribe function for prediction

client.on('message', (topic, message) => {
	if(topic === 'test/learn') {	 
       learn("702C1F405173","702C1F405173.txt");
	   //learn("123456789","123456789.txt");
	}
	else if(topic === 'mqtt/devapp/devid_B4E62DDCFE51/fromdev/predict')
	{
		console.log("msg received");
		// callName();
		var path= 'out.txt';
	    storeData(message,path,topic);
	}
  })


  app.post("/nilmfunctions",function(req,res){

    //console.log("data :: "+ JSON.stringify(req.body));
     if(req.body.op && req.body.hub_id)
     {
          switch(req.body.op )
          {
              case "1":
              //Doc:  writting in the file 
              if(req.body.trainningData)
              {
                //DOc: check the  data  is avalible in db are not based on hub id 
                var Models=db.getModels();
                Models.deviceSatusData.findAll({
                    where: {
                        hub_id:req.body.hub_id
                    }
                })
                .then(function(dataObj){
                    if(dataObj.length>=0)
                    {
                         console.log("length is" + dataObj.length);
                        var filename=req.body.hub_id+".txt";
                 try {
                    
                      //   file dose not exist create file and write the data to the  given file
                        var PromiseArray=[];
                        var obj=req.body;
                        obj.unique_id=generate_random_string(16);
                        var p = new Promise((resolve, reject) => { 

                            var trainarray=[];
                            trainarray=req.body.trainningData;
                            for(var i=0;i<trainarray.length;i++)
                        {
                            console.log("train array data :: " + trainarray[i]);
                            //Doc: traindata negitive value check 
                             
                             var trainSplit=trainarray[i].split(",")[0];
                             var dataAppend;
                             if(parseInt(trainSplit)>0)
                             {
                                dataAppend=(dataObj.length *2);
                                obj.on_detection=dataAppend;
                             }
                             else
                             {
                                dataAppend=(dataObj.length *2 )+1;
                                obj.off_detection=dataAppend;
                             }

                            var tuneData= dataAppend+","+trainarray[i]+"\n";
                            console.log("tunned data :: " + tuneData);
                            obj.trainningData=tuneData;
                        fs.appendFile('/home/blaze/Works/CCD/NILM/nilm/csvfile/'+filename, tuneData, function (err) { 
                            if (err)
                            {
                            res.send({status:0 ,message:"error in writting file " + err});
                            //reject
                            }
                            else
                            {
                                console.log('Write operation complete.');
                                
                                storeTriningData(obj);
                                  resolve(1);
                            }
                         });
                        }
                        PromiseArray.push(p)
                    }); 

                        Promise.all(PromiseArray).then(values => { 
                            console.log(values); 
                            
                            console.log("data before storing sattus data"+JSON.stringify(obj)); 
                                storeToDB(obj);
                            
                            // DOC: Creating  the .h5 file
                           //  createH5File(hub_id,filename);
                          
                            res.send({status:1 ,message:"Write operation complete."});
                           // learn(hub_id,filename);
                            learn("702C1F405173","702C1F405173.txt");
                           // learn("702C1F405173",req.body.hub_id+"702C1F405173.txt");

                          });
         
                  } catch(err) {
                    console.error(err)
                    res.send({status:0 ,message:"error in file checking " + err});
                  }
     
                    }
                    else
                    {
                        res.send({"status":0,message:"error in data base "});
                    }
                   
                })
                .catch(function(err){
                    res.send({"status":0,message:"error in data retriving error "+ err.message});
                })

              }
              else
              {
                res.send({status:0 ,message:"Training data is missing."})
              }
                        
              break;
              case "2":
              var filename=req.body.hub_id+".txt";
              try{

                var arr=req.body.traninigData;    
                var text = arr.join('\n');
            fs.appendFileSync('/home/blaze/Works/CCD/NILM/nilm/csvfile/'+filename, text, "utf8");
            res.send({status:1 ,message:"appending operation complete."})
            }
            catch(err)
            {
               
                    res.send({status:0 ,message:"error in appending file " + err});  

            } 
              break;
              case "3":
              fs.readFile('TestFile.txt', function (err, data) {
                        if (err) {
                            res.send({status:0 ,message:"error in reading file " + err});
                        }
                        else
                        {
                            res.send({status:1 ,message:data})
                        }

                        
                        });

              break;
              case "4":

                // fs.unlink('test.txt', function () {
        
                //     console.log('deletion operation complete.');
                
                // });
              res.send({status:0 ,message:"Deletion operation is not supported as of now"})
              break;
              case "5":
                // DOc: getting thed stastus data 
                var Models=db.getModels();
                Models.deviceSatusData.findAll({
                    where: {
                        hub_id:req.body.hub_id
                    }
                })
                .then(function(dataObj){
                   res.send({"status":1,message:"data retrived sucessfully",data:dataObj});
                })
                .catch(function(err){
                    res.send({"status":0,message:"error in data error "});
                })

              break;
          }

     }
     else
     {
         res.send({status:0 ,message:"Required parameters are missing"})
     }
  })


function storeToDB(obj)
{
    console.log("data to store " + JSON.stringify(obj));
    var saveObj={}
    // saveObj._id=generate_random_string(10); 
   // savaObj.trainningData=""+obj.trainningData;
    saveObj.hub_id=obj.hub_id;
    saveObj.unique_id=obj.unique_id;
    saveObj.modified_data=new Date();
    saveObj.device_name=obj.device_name;
    saveObj.device_status="OFF";
    saveObj.on_detection=""+obj.on_detection;
    saveObj.off_detection=""+obj.off_detection;
    //var Models=;
    db.getModels().deviceSatusData.build(saveObj).save()
    .then(function (data) {
        console.log("data saved sucessfully");
    })
    .catch(function(err){
        console.log("error in saving the data to db" +err.message );
    });
}

function storeTriningData(obj)
{
    console.log("data to store " + JSON.stringify(obj));
    var saveObj={}
    // saveObj._id=generate_random_string(10); 
    saveObj.trainningData=""+obj.trainningData;
    saveObj.hub_id=obj.hub_id;
    saveObj.unique_id=obj.unique_id;
    saveObj.modified_data=new Date();
    
    db.getModels().deviceTrainningData.build(saveObj).save()
    .then(function (data) {
        console.log("data saved sucessfully");
    })
    .catch(function(err){
        console.log("error in saving the data to db" +err.message );
    });
}

function storeData (message,path,topic)  {
	try {
        var res = message.toString().replace("NaN", 0);
        var input = JSON.parse(res);
       
	 console.log("message to storring  " + input.data);
   fs.writeFileSync(path, input.data)
     
   console.log("file completed");

   predict(topic,message);

	} catch (err) {
   console.error(err)
	}
   }


function predict(topic,message) { 
	var spawn = require("child_process").spawn; 
	var process = spawn('python',["./testSVM.py" ,"702C1F405173"
                            ] );
    var prdtmsg= message.toString();
	process.stdout.on('data', function(data) { 

        console.log("data is  comming for prediction " + data);
       return;
      /*  var predictdata=data.toString().substring(0,2);
        var mac_id=topic.toString().split("_")[1].substring(0,12);
        var isOff = prdtmsg.indexOf("-");
        var status;
        var device_name;
        var notimsg;
        if(isOff>0)
        {
            status="OFF";
            notimsg="Turned Off";
        }
        else
        {
            status="ON";
            notimsg="Turned On";
        }

        //Doc: inserting to history  for monog db

        //Doc: getting the unique id of the device pridicted

         var Models=db.getModels();
         
         Models.deviceTrainningData.findAll({
            where: ["trainningData like '%?%'",predictdata]

         })
         .then(function(preditdata){

            if(preditdata.length>0)
            {
                var unique_id=preditdata[0].unique_id;
             //Doc: finding the device details
                Models.deviceSatusData.findAll({where:{unique_id:unique_id}})
                .then(function(deviceData){
                    if(deviceData.length>0)
                    {
                      //Doc: updating the device status
                      device_name=deviceData[0].device_name;
                      var upObj={}
                      upObj.device_status=status;
                      Models.deviceSatusData.update(upObj, {
                        where: {
                            unique_id: unique_id
                    }})
                    .then(function (Updateddata) {
                        
                        // Object Send Notification to Mobile user 
                       var req={}
                       req.origin_id="36";

                        var pushoptions={
                            url:"smart.b1automation.com" + "/event/SetStatusWithPushforNest",
                            method: 'POST',
                            port: 443,
                            headers: {
                              'Content-Type': 'application/json',
                              'Accept': 'application/json',
                
                            },
                            json: req
                           }
                           console.log('nilm notification args '+JSON.stringify(pushoptions));
                           request(pushoptions, function (err, res, resbody) {
                            if (err) {
                
                             console.log("error in sending NILM notification " + JSON.stringify(resbody));   
                
                            } else {
                             console.log("NILM push had done sucessfully  " + JSON.stringify(resbody));
                            }
                
                           }); 

                    })
                }
                    else
                    {
                        console.log("device data not avalible for given unique id");
                    }
                })
            }
            else
            {
                console.log("prediction data not found"+err.message);
            }

         })
         .catch(function(err)
        {
            console.log("error in getting unique data of prdicted"+err.message);
        })*/


        //var nilmObj={};
        // nilmObj.mac_id=mac_id;
        // nilmObj.predictdata=predictdata;
        // nilmObj.message="sample data to store";
        // dbo.collection("nilm").insertOne(nilmObj, function(err, res) {
        //     if (err) throw err;
        //     console.log("1 document inserted");
        //     //db.close();
        //   });
    
        // DOC:  update the db with  the status 

        

	
		// console.log(data.toString()); 
	} ) 
} 

function learn(hub_id,filename) { 
    console.log("hi i am  comming " + filename);
	
	var spawn = require("child_process").spawn; 
	var process = spawn('python',["./Svmrbf.py" 
							,filename,hub_id] ); 
	process.stdout.on('data', function(data) { 

		console.log("data is  comming for learning" + data);
	
        // console.log(data.toString()); 
        

	} ) 
} 


function generate_random_string(string_length){
    let random_string = '';
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii)
    }
    return random_string
}

