var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(36),primaryKey: true ,defaultValue: Sequelize.UUIDV4},
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    trainningData: { type: Sequelize.STRING, defaultValue: null },
    devce_Sno: { type: Sequelize.INTEGER, autoIncrement: true,allowNull: false,unique: true },
    unique_id: { type: Sequelize.CHAR(16), allowNull: false ,defaultValue: Sequelize.UUIDV4},
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_data: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    
  };
};

exports.getTableName = function(){
  return "b1_trainning_data";
};
