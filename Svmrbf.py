#importing the libraries
import numpy as np
import matplotlib.pyplot as plt  

import pandas as pd
import sys
from sklearn import metrics,svm
#from sklearn.cross_validation import train_test_split,GridSearchCV
from sklearn.model_selection import train_test_split,GridSearchCV
from sklearn.metrics import accuracy_score,confusion_matrix

#import the datasets

#Train model text file 3_loads.txt change it for new train set
print("comming opening the text file")
print("First name: " + sys.argv[1]) 
print("Last name: " + sys.argv[2])
print(type(sys.argv[1]))
path ='/home/blaze/Works/CCD/NILM/nilm/csvfile/'+sys.argv[2]+'.txt'
print(path)
dataset =pd.read_csv(path, header = None)
X = dataset.iloc[:,1:].values
Y = dataset.iloc[:,0].values

# Encoding categorical data
# Encoding the Independent Variable
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder

labelencoder_Y = LabelEncoder()
Y = labelencoder_Y.fit_transform(Y)
#onehotencoder = OneHotEncoder(categorical_features = [0])
#Y = onehotencoder.fit_transform(Y).toarray()

#from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X,Y,test_size = 0.3,random_state = 10)
print("X_train")
print(X_train)
print("Y_train")
print(Y_train)

svc=svm.SVC(C=10,gamma=0.00000000001,decision_function_shape='ovr',kernel="rbf",random_state=10)
svc.fit(X_train, Y_train)
# save model
import pickle

#model Save .h5 is the model for predection
pickle.dump(svc,open('./'+sys.argv[2]+".h5", 'wb'))

# predict value by loading saved model
rdtest= pickle.load(open('./'+sys.argv[2]+".h5", 'rb'))
#print("pickle2")
Y_pred = rdtest.predict(X_test)
#print("pickle3")
print(Y_pred)
#print("pickle End")

# check accuracy
print("")
print("training accuracy_score  :"+str(rdtest.score(X_train,Y_train)))
print("testing accuracy_score   :"+str(accuracy_score(Y_test,Y_pred)))
print("")
print("confusion_matrix")
print(confusion_matrix(Y_test, Y_pred))
print("")
#print("all done")
print("Classification report for - \n{}:\n{}\n".format(
    rdtest, metrics.classification_report(Y_test, Y_pred)))
#print("end")
