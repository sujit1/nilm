var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(36),primaryKey: true  },
    device_name: { type: Sequelize.STRING, defaultValue: null },
    devce_status: { type: Sequelize.STRING, defaultValue: "ON" },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_data: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
    
  };
};

exports.getTableName = function(){
  return "b1_test_data";
};
