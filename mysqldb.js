var Sequelize = require('sequelize');
var deviceTrainningData = require('./deviceTrainningData');
var deviceSatusData = require('./deviceSatusData');
var testData=require("./testdata");
var mySqlConnection = null;
var models = {};



exports.connect = function (host, port, dbName, userName, password, cb) {
   
    var sequelize = new Sequelize(dbName, userName, password, {
        // default db values
        // host: localhost,
        // port: 3306,
        host: host,
        port: port,
        dialect: 'mysql',
        logging: false,
        syncOnAssociation: true,
        pool: {
            max: 50,
            min: 10,
            idle: 60000
        },
        define: {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            timestamps: false
        }
    });
    models.deviceTrainningData = sequelize.define('deviceTrainningData', deviceTrainningData.getSchema(), {
        tableName: deviceTrainningData.getTableName()
    });
    models.deviceSatusData = sequelize.define('deviceSatusData', deviceSatusData.getSchema(), {
        tableName: deviceSatusData.getTableName()
    });

    models.testData = sequelize.define('testData', testData.getSchema(), {
        tableName: testData.getTableName()
    });
    
    // sequelize.sync({force:true}).then(function(){    // Note: This will delete the tables and create them again.
    sequelize.sync({
        alter: true
    }).then(function () {
        mySqlConnection = sequelize;
        cb();
    }).catch(function (err) {
        console.log("Error while connecting to Db: " + err.message);
    });



};

exports.getModels = function () {
    return models;
};

