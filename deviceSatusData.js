var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    device_name: { type: Sequelize.STRING, defaultValue: null },
    device_status: { type: Sequelize.STRING, defaultValue: "OFF"},
    unique_id: { type: Sequelize.CHAR(16)},
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_data: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    on_detection:{ type: Sequelize.STRING },
    off_detection:{type: Sequelize.STRING}
    
  };
};

exports.getTableName = function(){
  return "b1_status_data";
};
